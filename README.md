# eslint

[ESlint](https://eslint.org/) finds and fixes problems in your JavaScript and
vue code, and your json files.

## Using in the Pipeline

To enable eslint in your project's pipeline add `eslint` to the `ENABLE_JOBS`
variable in your project's `.gitlab-ci.yaml` e.g.:

```bash
variables:
  ENABLE_JOBS: "eslint"
```

The `ENABLE_JOBS` variable is a comma-separated string value, e.g.
`ENABLE_JOBS="job1,job2,job3"`

## Using in Visual Studio Code

Install the
[ESlint extension](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
for Visual Studio Code.

## Using Locally in Your Development Environment

ESlint can be run locally in a linux-based bash shell or in a linting shell
script with this Docker command:

```bash
docker run -v "${PWD}":/workdir -w /workdir
    registry.gitlab.com/librefoodpantry/common-services/tools/linters/eslint:latest
    eslint --ext .js,.vue,.json --color .
```

You can include as many, or as few, file extensions (`.js`, `.vue`, or `.json`)
in the command above as appropriate for the types of files in your project.

## Configuration

Each project that uses the eslint tool must have a `.eslintrc.json` file which
is used for eslint configuration.

This project contains an example `.eslintrc.json` file. See the
[eslint configuration documentation](https://eslint.org/docs/latest/use/configure/)
if you want to make changes.

If a project uses vue, you should
[add the vue plugin](https://eslint.vuejs.org/user-guide/#configuration)
to the `.eslintrc.js` file.

This file is used by the pipeline tool, the Visual Studio Code extension,
and the locally run Docker image.

## Licensing

* Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
* Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
* Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
