#!/usr/bin/env bash

set -e

mkdir -p ./artifacts/release
echo "# Build ${1}" >> ./artifacts/release/eslint.yml
cat ./source/gitlab-ci/eslint.yml >> ./artifacts/release/eslint.yml
